// console.log("Activity 33 here");

console.log(fetch("https://jsonplaceholder.typicode.com/todos"));

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(data => {
	let title = data.map(element => element.title)
	console.log(title);
})

// async function fetchData(){
// 	let result = await(fetch("https://jsonplaceholder.typicode.com/todos"))

// 	console.log(result);

// 	let json = await result.json();
// 	console.log(json);
// }

// fetchData();


fetch("https://jsonplaceholder.typicode.com/todos/1", {method: "GET"})
.then(response => response.json())
.then(result => console.log(`The item "${result.title}" on the list has a status of ${result.completed}`));





fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		id: 1,
		title: "Created to do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(result => console.log(result));



fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated to do List Item",
		description: "To update the to do list with a different data structure",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1
	})
})
.then(response => response.json())
.then(result => console.log(result));



fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		status: "Complete",
		title: "delectus aut autem",
		userId: 1
	})
})
.then(response => response.json())
.then(result => console.log(result));



fetch("https://jsonplaceholder.typicode.com/todos/3", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: true,
		dateCompleted: "01/31/23"
	})
})
.then(response => response.json())
.then(result => console.log(result));




fetch("https://jsonplaceholder.typicode.com/todos/4", {
	method: "DELETE"
})
.then(response => response.json())
.then(result => console.log(result));